<?php

declare(strict_types=1);

namespace internTask\Strategy;

use internTask\Interface\CommissionCalculatorStrategyInterface;

class BusinessWithdrawCommissionCalculatorStrategy implements CommissionCalculatorStrategyInterface
{
    const WITHDRAW_BUSINESS_COMMISSION_RATE = 0.005;

    public function calculateCommission(string $date, string $userId, float $amount, string $currency): float
    {
        $commissionRate = self::WITHDRAW_BUSINESS_COMMISSION_RATE;

        return $amount * $commissionRate;
    }
}
