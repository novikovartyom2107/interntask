<?php

declare(strict_types=1);

namespace internTask\Strategy;

use internTask\Interface\CommissionCalculatorStrategyInterface;

class DepositCommissionCalculatorStrategy implements CommissionCalculatorStrategyInterface
{
    const DEPOSIT_COMMISSION_RATE = 0.0003;

    public function calculateCommission(string $date, string $userId, float $amount, string $currency): float
    {
        $commissionRate = self::DEPOSIT_COMMISSION_RATE;

        return $amount * $commissionRate;
    }
}
