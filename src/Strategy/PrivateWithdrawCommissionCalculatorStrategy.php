<?php

declare(strict_types=1);

namespace internTask\Strategy;

use Exception;
use internTask\Interface\CommissionCalculatorStrategyInterface;
use internTask\Repository\WithdrawalRepository;
use internTask\Service\RateService;

class PrivateWithdrawCommissionCalculatorStrategy implements CommissionCalculatorStrategyInterface
{
    const WITHDRAW_PRIVATE_COMMISSION_RATE = 0.003;

    public function __construct(
        protected RateService $rateService,
        protected WithdrawalRepository $withdrawalRepository
    ) {
    }

    /**
     * @throws Exception
     */
    public function calculateCommission(string $date, string $userId, float $amount, string $currency): float
    {
        $weekKey = $this->withdrawalRepository->initializeWeekWithdrawalData($date, $userId);
        $this->withdrawalRepository->incrementWithdrawalCount($weekKey);
        $balance = $this->withdrawalRepository->getWeekWithdrawalBalance($weekKey);

        if ($this->withdrawalRepository->isWithinWithdrawalLimit($weekKey)) {
            return $this->calculateCommissionWithinWithdrawLimit($amount, $balance, $currency, $weekKey);
        } else {
            return $this->calculateCommissionExceedingWithdrawLimit($amount);
        }
    }

    private function calculateCommissionWithinWithdrawLimit(
        float $amount,
        float $balance,
        string $currency,
        string $weekKey
    ): float {
        if ($amount <= $balance) {
            $this->withdrawalRepository->updateWeekWithdrawalBalance($weekKey, $amount);

            return 0;
        }

        $balance = max(0, $balance);
        $currencyExchangeRate = $this->rateService->getCurrencyCommissionRate($currency);
        $exceededAmount = $amount - $balance * $currencyExchangeRate;
        $commissionRate = self::WITHDRAW_PRIVATE_COMMISSION_RATE;
        $commission = $exceededAmount * $commissionRate;

        // Check if commission is negative, set it to 0 and update the withdrawal balance
        if ($commission < 0) {
            $commission = 0;
            $this->withdrawalRepository->updateWeekWithdrawalBalance($weekKey, $amount / $currencyExchangeRate);

            return $commission;
        }

        $this->withdrawalRepository->updateWeekWithdrawalBalance($weekKey, $amount);

        return $commission;
    }

    private function calculateCommissionExceedingWithdrawLimit(float $amount): float
    {
        $commissionRate = self::WITHDRAW_PRIVATE_COMMISSION_RATE;

        return $amount * $commissionRate;
    }
}
