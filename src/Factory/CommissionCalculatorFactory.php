<?php

declare(strict_types=1);

namespace internTask\Factory;

use Exception;
use internTask\Interface\CommissionCalculatorStrategyInterface;
use internTask\Repository\WithdrawalRepository;
use internTask\Service\RateService;
use internTask\Strategy\BusinessWithdrawCommissionCalculatorStrategy;
use internTask\Strategy\DepositCommissionCalculatorStrategy;
use internTask\Strategy\PrivateWithdrawCommissionCalculatorStrategy;

class CommissionCalculatorFactory
{
    /**
     * @throws Exception
     */
    public function createWithdrawalStrategy(
        string $userType,
        RateService $rateService,
        WithdrawalRepository $withdrawalRepository
    ): CommissionCalculatorStrategyInterface {
        if ($userType === 'private') {
            return new PrivateWithdrawCommissionCalculatorStrategy($rateService, $withdrawalRepository);
        } elseif ($userType === 'business') {
            return new BusinessWithdrawCommissionCalculatorStrategy();
        } else {
            throw new Exception("Invalid user type: $userType");
        }
    }

    public function createDepositStrategy(): DepositCommissionCalculatorStrategy
    {
        return new DepositCommissionCalculatorStrategy();
    }
}