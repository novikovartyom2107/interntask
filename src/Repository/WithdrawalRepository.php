<?php

declare(strict_types=1);

namespace internTask\Repository;

use internTask\Interface\WithdrawalRepositoryInterface;
use internTask\Service\DataStorageService;

class WithdrawalRepository implements WithdrawalRepositoryInterface
{
    const MAX_WITHDRAWAL_COUNT = 3;

    public function __construct(
        protected DataStorageService $dataStorage
    ) {
    }

    public function initializeWeekWithdrawalData(string $date, string $userId): string
    {
        $weekStartDate = date('Y-m-d', strtotime("this week Monday", strtotime($date)));
        $weekEndDate = date('Y-m-d', strtotime("next week Monday", strtotime($date)));

        $weekKey = $userId . '-' . $weekStartDate . '-' . $weekEndDate;
        if (!$this->dataStorage->retrieveData($weekKey)) {
            $this->dataStorage->storeData($weekKey, [
                'balance' => 1000,
                'count' => 0
            ]);
        }

        return $weekKey;
    }

    public function incrementWithdrawalCount(string $weekKey): void
    {
        $data = $this->dataStorage->retrieveData($weekKey);
        $data['count']++;
        $this->dataStorage->storeData($weekKey, $data);
    }

    public function getWeekWithdrawalBalance(string $weekKey): float
    {
        $data = $this->dataStorage->retrieveData($weekKey);

        return $data['balance'] ?? 0;
    }

    public function updateWeekWithdrawalBalance(string $weekKey, float $amount): void
    {
        $data = $this->dataStorage->retrieveData($weekKey);
        $data['balance'] -= $amount;
        $this->dataStorage->storeData($weekKey, $data);
    }

    public function isWithinWithdrawalLimit(string $weekKey): bool
    {
        $data = $this->dataStorage->retrieveData($weekKey);

        return $data['count'] <= self::MAX_WITHDRAWAL_COUNT;
    }
}
