<?php

declare(strict_types=1);

namespace internTask\Interface;

interface CommissionCalculatorStrategyInterface
{
    public function calculateCommission(string $date, string $userId, float $amount, string $currency): float;
}