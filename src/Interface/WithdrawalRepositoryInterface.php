<?php

declare(strict_types=1);

namespace internTask\Interface;

interface WithdrawalRepositoryInterface
{
    public function initializeWeekWithdrawalData(string $date, string $userId): string;

    public function incrementWithdrawalCount(string $weekKey): void;

    public function getWeekWithdrawalBalance(string $weekKey): float;

    public function updateWeekWithdrawalBalance(string $weekKey, float $amount): void;

    public function isWithinWithdrawalLimit(string $weekKey): bool;
}