<?php

declare(strict_types=1);

namespace internTask\Service;

use Exception;

class RateService
{
    /**
     * @throws Exception
     */
    public function retrieveRates(): array
    {
        $ratesPath = dirname(__FILE__, 3);
        $jsonData = file_get_contents($ratesPath . '/rates.json');
        if ($jsonData === false) {
            throw new Exception("Failed to read JSON data from the file");
        }

        $rates = json_decode($jsonData, true);

        return $rates['rates'];
    }

    public function getCurrencyCommissionRate(string $currency){
        $rates = $this->retrieveRates();
        return $rates[$currency];
    }
}
