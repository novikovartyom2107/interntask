<?php

declare(strict_types=1);

namespace internTask\Service;

use Exception;
use internTask\Interface\WithdrawalRepositoryInterface;
use internTask\Interface\CommissionCalculatorStrategyInterface;
use internTask\Factory\CommissionCalculatorFactory;

class CommissionCalculator
{
    const DEFAULT_PRECISION = 2;

    const CURRENCY_PRECISIONS = [
        'JPY' => 0,
        'KRW' => 0,
        'CLP' => 0,
        'PYG' => 0,
        'VND' => 0,
        'IDR' => 0,
        'IQD' => 0,
        'RWF' => 0,
        'GNF' => 0,
        'KMF' => 0
    ];

    public function __construct(
        protected RateService $rateService,
        protected CsvParser $csvParser,
        protected WithdrawalRepositoryInterface $withdrawalRepository,
        protected CommissionCalculatorFactory $commissionCalculatorFactory
    ) {
    }

    /**
     * @throws Exception
     */
    public function getCommissions(string $filename): array
    {
        $lines = $this->csvParser->parse($filename);
        $commissions = [];

        foreach ($lines as $line) {
            $commission = $this->calculateCommission(...$line);
            $commissions[] = $this->formatCommission($line[5], $commission);
        }
        return $commissions;
    }

    /**
     * @throws Exception
     */
    private function calculateCommission(
        string $date,
        string $userId,
        string $userType,
        string $operation,
        $amount,
        string $currency
    ): float {
        $amount = floatval($amount);

        if ($operation === 'withdraw') {
            $calculatorStrategy = $this->getWithdrawalStrategy($userType);
        } elseif ($operation === 'deposit') {
            $calculatorStrategy = $this->getDepositStrategy();
        } else {
            throw new Exception("Invalid operation: $operation");
        }
        return $calculatorStrategy->calculateCommission($date, $userId, $amount, $currency);
    }

    public function formatCommission(string $currency, float $commission): float
    {
        $precision = self::CURRENCY_PRECISIONS[$currency] ?? self::DEFAULT_PRECISION;

        if ($precision === 0) {
            return ceil($commission);
        }

        return ceil($commission * pow(10, $precision)) / pow(10, $precision);
    }

    /**
     * @throws Exception
     */
    private function getWithdrawalStrategy(string $userType): CommissionCalculatorStrategyInterface
    {
        return $this->commissionCalculatorFactory->createWithdrawalStrategy($userType, $this->rateService,
            $this->withdrawalRepository);
    }

    private function getDepositStrategy(): CommissionCalculatorStrategyInterface
    {
        return $this->commissionCalculatorFactory->createDepositStrategy();
    }
}
