<?php

declare(strict_types=1);

namespace internTask\Service;

class DataStorageService
{
    protected array $data;

    public function __construct()
    {
        $this->data = [];
    }

    public function storeData(string $key, array $transactions): void
    {
        $this->data[$key] = $transactions;
    }

    public function retrieveData(string $key): array
    {
        return $this->data[$key] ?? [];
    }
}
