<?php

declare(strict_types=1);

namespace internTask\Service;

use Exception;

class CsvParser
{
    public function parse(string $filename): array
    {
        $file = fopen($filename, 'r');

        if ($file === false) {
            throw new Exception("Failed to open the CSV file.");
        }

        $lines = $this->readCsvLines($file);
        fclose($file);

        return $lines;
    }

    private function readCsvLines($file): array
    {
        $lines = [];
        while (($row = fgetcsv($file)) !== false) {
            $lines[] = $row;
        }

        return $lines;
    }
}
