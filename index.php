<?php

declare(strict_types=1);

namespace internTask;

require __DIR__ . '/vendor/autoload.php';

use internTask\Factory\CommissionCalculatorFactory;
use internTask\Repository\WithdrawalRepository;
use internTask\Service\CommissionCalculator;
use internTask\Service\CsvParser;
use internTask\Service\DataStorageService;
use internTask\Service\RateService;

$rateService = new RateService();
$csvParser = new CsvParser();
$dataStorageService = new DataStorageService();
$commissionCalculatorFactory = new CommissionCalculatorFactory();
$withdrawalRepository = new WithdrawalRepository($dataStorageService);
$calculator = new CommissionCalculator($rateService, $csvParser, $withdrawalRepository, $commissionCalculatorFactory);

if (isset($argv[1])) {
    $inputPath = $argv[1];
    $commissions = ($calculator->getCommissions($inputPath));
    foreach ($commissions as $commission){
        echo $commission . PHP_EOL;
    }
} else {
    echo "Please provide the CSV file path as a command-line argument.";
}
