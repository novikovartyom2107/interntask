# internship task

## installation

To make it easy for you to get started with the project, here's a list of recommended next steps:

1) To install all needed dependencies, run
   ```bash
   composer install
   ```

2) To run script, run:
   ```bash
   php index.php transactions.csv
   ```

3) To run tests:
   ```bash
   composer run test
   ```

## Implementation

The CommissionCalculator is implemented using a Factory pattern for creating instances of the commission calculation
strategies. The code is organized into a separate CommissionCalculatorFactory class, which encapsulates the logic for
strategy creation.

By adopting the Factory pattern, the code benefits from improved flexibility and modularity. The creation of commission
calculation strategies is delegated to the CommissionCalculatorFactory, reducing coupling and allowing for easier
extension and modification in the future.

### Advantages:

- Code organization: Each method performs a specific task, making it easier to understand and modify the code in the
  future.

- Re-usability: Class methods can be reused by instantiating the CommissionCalculator object and calling the appropriate
  methods.

- Testability: The code can be easily tested by creating unit tests for each method to ensure the correctness of
  individual components.

- PSR-12 compatible: The code follows the PSR-12 coding style standard, enhancing readability and maintainability.

- Replaced inheritance with composition.

- Easy installation and execution: The code can be easily installed and executed.

- This approach centralizes the instantiation logic, promoting cleaner code organization and better separation of
  concerns. It also enables easier testing and ensures that the code adheres to the SOLID principles.

### Drawbacks:

- Complicates the program due to additional classes.
- The client must know what the difference between the strategies is in order to choose the right one.
- Factory pattern might introduce unnecessary complexity if the number of calculator subclasses is fixed and known in
  advance.

## Other possible implementations

### Object-Oriented Approach.

#### Advantages

- This involves defining classes for different entities such as Transaction, User, and CommissionRate, encapsulating
  their
  properties and behaviors.

- This approach can promote better encapsulation, separation of concerns, and code re-usability.

#### Drawbacks

- Will cause additional complexity and overhead, especially for such small projects.

- No really flexible

### Procedural Approach

#### Advantages

- Simplicity. Procedural approach is generally simpler and easier to implement. It is suitable for smaller programs or
  scripts that don't require extensive code organization.

#### Drawbacks

- Code Duplication. Related functions may need to be copied and modified for different use cases, leading to code
  redundancy.

- Procedural code can be less reusable since functions and procedures are often tightly coupled with
  specific data structures.

- Data and functions are not bundled together in objects, making it harder to enforce data integrity and control access
  to data.

- Procedural code can become challenging to maintain and scale as the program grows larger and more
  complex.
- The absence of a modular structure can make it harder to manage dependencies and enforce code organization.

## Assumptions possible during code operation.

The code assumes that the supported operations are limited to "withdraw" and "deposit". It throws an exception if any
other operation is encountered. But code is flexible enough to easily add a new operation.

The code assumes that the input CSV file follows a specific format where each line represents a transaction with fields
in a specific order: date, user ID, user type, operation, amount, and currency.