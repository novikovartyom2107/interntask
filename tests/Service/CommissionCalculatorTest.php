<?php

declare(strict_types=1);

namespace internTask\Tests\Service;

use Exception;
use internTask\Factory\CommissionCalculatorFactory;
use internTask\Repository\WithdrawalRepository;
use internTask\Service\CommissionCalculator;
use internTask\Service\CsvParser;
use internTask\Service\DataStorageService;
use internTask\Service\RateService;
use PHPUnit\Framework\TestCase;

class CommissionCalculatorTest extends TestCase
{
    /**
     * @throws Exception
     */
    public function testCommissionCalculation()
    {
        $rateService = new RateService();
        $csvParser = new CsvParser();
        $dataStorageService = new DataStorageService();
        $commissionCalculatorFactory = new CommissionCalculatorFactory();
        $withdrawalRepository = new WithdrawalRepository($dataStorageService);
        $calculator = new CommissionCalculator($rateService, $csvParser, $withdrawalRepository, $commissionCalculatorFactory);

        $inputPath = dirname(__FILE__, 2);
        $expectedCommissions = [0.6, 3, 0, 0.06, 1.5, 0.0, 0.69, 0.3, 0.3, 3, 0, 0, 8608];
        $commissions = $calculator->getCommissions($inputPath . '/input.csv');

        $this->assertEquals($expectedCommissions, $commissions);
    }
}
